#pragma once

#include "cocostudio/CocoStudio.h"

class SceneManager : public cocos2d::Scene
{
public:
	SceneManager();
	~SceneManager();
	
	static cocos2d::Scene* create();
};

