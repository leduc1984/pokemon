#pragma once

#include "cocos2d.h"
#include <iostream>
#include "proj.win32/characters/Trainer.h"

using namespace cocos2d;
using namespace std;

#ifndef TILE_ABOVE
	#define TILE_ABOVE 15
#endif // !TILE_ABOVE
#ifndef TILE_PLAYER
	#define TILE_PLAYER 3
#endif // !TILE_PLAYER

//stock information about collision (position and size)
struct Collision
{
	Collision(float _x, float _y, float _width, float _height)
	{
		x = _x;
		y = _y;
		width = _width;
		height = _height;
	}

	float x;
	float y;
	float width;
	float height;
};


class MapManager : public Node
{

public:
	MapManager();
	MapManager(std::string mapName);
	MapManager(int id, std::string mapName);
	~MapManager();


	int getId();
	std::string getMapName();
	TMXTiledMap* getMapInformation();
	static TMXTiledMap* getMapInfo();
	static vector<struct Collision> getCollisions();
	static vector<struct Collision> getFightZone();
	static void addPlayerToTheMap(Character* character);
	static bool getCollisionForCharacter(class Character* chara);
	static bool checkFightForCharacter(class Trainer* chara);
private:

	/* PRIVATE PROPERTIES */
	int id;
	std::string mapName;
	std::string mapFile;
	TMXTiledMap *mapInformation;
	Scene *scene;

	/* CONSTANT */
	static std::string MAPS_DIRECTORY_PATH;
	static float TILE_SIZE;
	static TMXTiledMap *mapInfo;
	static vector<struct Collision> collisions;
	static vector<struct Collision> fightZone;
	
	/* PRIVATE METHODS */
	void createMap();
	
};

