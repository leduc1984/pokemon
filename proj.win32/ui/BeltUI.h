#pragma once

#include "cocos2d.h"
#include "proj.win32/pokemon/Pokemon.h"

using namespace cocos2d;


class BeltUI : public Node
{
public:
	BeltUI();
	~BeltUI();

	static void create();
	
	void refreshBeltContent(vector<Pokemon> *pokemonBelt);
};

