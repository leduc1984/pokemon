#include "BeltUI.h"
#include "ui\UIImageView.h"
#include "proj.win32\ui\UIManager.h"


BeltUI::BeltUI()
{
	
}

BeltUI::~BeltUI()
{

}

void BeltUI::create()
{
	auto beltUI = new BeltUI();

	auto background = Sprite::create("ui/belt.png");
	if (background)
	{

		auto width = Director::getInstance()->getWinSize().width;
		auto height = Director::getInstance()->getWinSize().height;

		beltUI->setPosition(Vec2(width - (background->getContentSize().width/2), 30));
		beltUI->addChild(background, 0, "background");

		auto uiManager = UIManager::getInstance();
		if (uiManager)
		{
			//add to the UIManager
			uiManager->addChild(beltUI, UI_ZORDER, "pokemon_belt");
		}			
	}
}

void BeltUI::refreshBeltContent(vector<Pokemon> *pokemonBelt)
{
	int i = 1;
	for (auto &pokemon = pokemonBelt->begin(); pokemon != pokemonBelt->end(); ++pokemon)
	{
		if (pokemon->getMiniThumb())
		{
			//get background size
			auto background = this->getChildByName("background");

			this->addChild(pokemon->getMiniThumb(), 1);
			pokemon->getMiniThumb()->setPosition(-((32  * i) - background->getContentSize().width/2) + 5, 0);
		}
		i++;
	}
}

