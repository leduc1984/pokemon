#pragma once

#include "cocos2d.h"
#include <iostream>

using namespace cocos2d;
using namespace std;

class Character : public Node
{

public:
	Character(std::string name, Sprite *sprite);
	Character(int id, std::string name, Sprite *sprite);
	~Character();

	Sprite* getSprite();
	void setSprite(Sprite *sprite);


private:

	int id;
	std::string name;
	Sprite *sprite;
};

